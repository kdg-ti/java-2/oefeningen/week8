import apen.Aap;
import conversie.ConversieTools;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class DemoApen {
    public static void main(String[] args) {
        try {
            List<Aap> myApenList = ConversieTools.GsonReadList("AlleApen.json");

            System.out.println("Alle apen per leeftijd:");
            //Todo 3B sorteer via stream op leeftijd en druk af met formaat
	          //  naam       --> leeftijd


            //Todo 3C groepeer kleine en grote apen volgens de eerste letter van het
	        // kooinummer
            Map<Boolean, List<Aap>> myMap = null; //vul aan


            ConversieTools.GsonWriteList(myMap.get(true), "KleineApen.json");

            List<Aap> newList = ConversieTools.GsonReadList("KleineApen.json");
            System.out.println("\nIngelezen JSON data (alle kleine apen):");
            for (Aap aap : newList) {
                System.out.println(aap);
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}

/**
 * GEWENSTE AFDRUK:
 *
 * Alle apen per leeftijd:
 Griezel      --> 2 jaar
 Banana       --> 5 jaar
 Java         --> 5 jaar
 Bumba        --> 8 jaar
 Nikita       --> 8 jaar
 Pipi         --> 10 jaar
 Pinky        --> 11 jaar
 Pinokkio     --> 12 jaar
 Monkey       --> 13 jaar
 Grompy       --> 16 jaar
 Nancy        --> 16 jaar
 Shout        --> 18 jaar
 Louie        --> 23 jaar
 Rosa         --> 23 jaar
 Sneeuwvlokje --> 23 jaar
 Kingkong     --> 33 jaar
 Koko         --> 34 jaar
 Gust         --> 35 jaar

 Ingelezen JSON data (alle kleine apen):
 Banana          maki            halfapen          V   geboren: 2010     1,2 kg   kooi: K010
 Bumba           doodshoofdaap   kapucijnapen      V   geboren: 2007     0,7 kg   kooi: K008
 Griezel         doodshoofdaap   kapucijnapen      V   geboren: 2012     0,5 kg   kooi: K008
 Grompy          brulaap         grijpstaartapen   M   geboren: 1999     7,2 kg   kooi: K006
 Java            leeuwaap        klauwapen         M   geboren: 2010     0,4 kg   kooi: K034
 Monkey          leeuwaap        klauwapen         M   geboren: 2002     0,7 kg   kooi: K033
 Nancy           neusaap         bladapen          V   geboren: 1999    13,0 kg   kooi: K014
 Nikita          maki            halfapen          V   geboren: 2007     1,1 kg   kooi: K010
 Pinokkio        neusaap         bladapen          M   geboren: 2003    11,0 kg   kooi: K016
 Shout           brulaap         grijpstaartapen   M   geboren: 1997     6,0 kg   kooi: K006

 */
