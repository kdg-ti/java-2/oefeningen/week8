import apen.Aap;
import apen.Apen;
import conversie.ConversieTools;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class DemoApen {
    public static void main(String[] args) {
        try {
            Apen apen = ConversieTools.JaxbReadXML("AlleApen.xml");
            List<Aap> myApenList = apen.getApenList();

	        //Todo 2B:sorteer  de data in apenlist op naam, groepeer ze per soort en stop
	        // ze in soortMap. druk de map af            Map<String, List<Aap>> soortMap = null;
	        Map<String, List<Aap>> soortMap =null;
		        System.out.println("\nAlle apen alfabetisch per soort:");


            //Opgave 2C:Maak opnieuw gebruik van Stream en zoek de zwaarste mannetjesaap.

	        Aap zwaarste = null;
            System.out.printf("\nZwaarste mannetjesaap: %s = %.1f kg\n"
                    , zwaarste.getNaam(), zwaarste.getGewicht());

            ConversieTools.StaxWriteXML(soortMap, "ApenPerSoort.xml");

        } catch (JAXBException | XMLStreamException | IOException
	        e) {
            System.out.println(e.getMessage());
        }
    }
}

/**
 * GEWENSTE AFDRUK:
 *
 Alle apen alfabetisch per soort:
 bonobo   -> Koko, Pipi
 brulaap  -> Grompy, Shout
 doodshoofdaap -> Bumba, Griezel
 gorilla  -> Gust, Pinky, Sneeuwvlokje
 leeuwaap -> Java, Monkey
 maki     -> Banana, Nikita
 neusaap  -> Nancy, Pinokkio
 orang-oetan -> Kingkong, Louie, Rosa

 Zwaarste mannetjesaap: Gust = 175,0 kg
 File saved!
 */
