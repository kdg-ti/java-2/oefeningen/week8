package conversie;

import apen.Aap;
import apen.Apen;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ConversieTools {
	public static Apen JaxbReadXML(String fileName) throws JAXBException {
		//TODO 2A lees met  jaxb de data uit filename en geeft terug als Apen object

		return null;
	}


	public static void StaxWriteXML(Map<String, List<Aap>> myMap, String fileName) throws XMLStreamException, IOException {
		FileWriter writerXml = new FileWriter( (fileName));
		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(writerXml);
		//Specifieke xmlStreamWriter om indenting in de XML-output in te voegen:
		xmlStreamWriter.writeStartDocument();

		//TODO 2D Maak een root element <apen>
		// Van elk Aap-object schrijf je enkel de eigenschappen naam, gewicht, geboorte en kooi weg.



		xmlStreamWriter.close();
		writerXml.close();

		System.out.println("File saved!");
	}
}