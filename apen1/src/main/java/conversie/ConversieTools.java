package conversie;

import apen.Aap;
import apen.Geslacht;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class ConversieTools {

    public static List<Aap> readTxtFile(String bestand) throws IOException {
        List<Aap> list = new ArrayList<>();
        String regel = "";
        try (BufferedReader br = new BufferedReader(new FileReader(bestand))) {
            while ((regel = br.readLine()) != null) {
                //Elke regel in het bestand bevat de data van een aap, gescheiden door komma's
                StringTokenizer tokenizer = new StringTokenizer(regel, ",");

                String naam = tokenizer.nextToken();
                String soort = tokenizer.nextToken();
                String familie = tokenizer.nextToken();
                Geslacht geslacht = tokenizer.nextToken().charAt(0) == 'M' ? Geslacht.MAN : Geslacht.VROUW;
                LocalDate geboorte = LocalDate.parse(tokenizer.nextToken());
                double gewicht = Double.parseDouble(tokenizer.nextToken());
                String kooi = tokenizer.nextToken();

                // Nieuwe aap maken en toevoegen aan de List:
                list.add(new Aap(naam, soort, familie, geslacht, geboorte, gewicht, kooi));
            }
            return list;
        } catch (NoSuchElementException | NumberFormatException e1) {
            throw new IOException("Leesfout in regel: " + regel, e1);
        } catch (IOException e2) {
            throw new IOException("Het bronbestand " + bestand + " kan niet geopend worden", e2);
        }
    }

    public static String DomWriteXML(List<Aap> myList, String fileName) throws IOException {
        //Opgave 1C
        return "";
    }
}
